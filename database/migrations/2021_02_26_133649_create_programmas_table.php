<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgrammasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug');
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->date('date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('presentor_id')->nullable();
            $table->foreign('presentor_id')->references('id')->on('presentors')->onDelete('cascade');
            $table->unsignedBigInteger('zenders_id')->nullable();
            $table->foreign('zenders_id')->references('id')->on('zenders')->onDelete('cascade');
        });

        \App\Programmas::create([
            'title' => 'Soulshow',
            'slug' => 'Soulshow',
        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programmas');
    }
}
