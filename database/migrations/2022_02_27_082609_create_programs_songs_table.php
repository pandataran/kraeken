<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programmas_songs', function (Blueprint $table) {
            $table->unsignedBigInteger('programmas_id');
            $table->foreign('programmas_id')->references('id')->on('programmas')->onDelete('cascade');
            $table->unsignedBigInteger('songs_id');
            $table->foreign('songs_id')->references('id')->on('liedjes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs_songs');

    }
}
