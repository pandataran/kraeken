<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} {{ app()->version() }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma-spacing/css/bulma-spacing.min.css">

</head>
<body>
<div id="app">
    <nav class="navbar">
        <div class="navbar-brand">
            <a class="navbar-item" href="/">Logo</a>
        </div>
        <div>
        </div>
    </nav>
    <nav class="navbar is-link">
        <div class="navbar-brand">
            @if (Auth::guest())
                <a class="navbar-item" href="/">Home</a>
                <a class="navbar-item" href="/contact">Contact</a>
                <a class="navbar-item" href="/zenders">Zenders</a>
            @else
                <a class="navbar-item" href="/">Home</a>
                <a class="navbar-item" href="/contact">Contact</a>
                <a class="navbar-item" href="/zenders">Zenders</a>
                <a class="navbar-item" href="/liedjes">Liedjes</a>
                <a class="navbar-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
            @endif
        </div>
        <div>
        </div>
    </nav>
    @yield('content')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

{{--@if (Auth::guest())--}}
{{--    <a class="navbar-item " href="{{ route('login') }}">Login</a>--}}
{{--    <a class="navbar-item " href="{{ route('register') }}">Register</a>--}}
{{--@else--}}
{{--    <div class="navbar-item has-dropdown is-hoverable">--}}
{{--        <a class="navbar-link" href="#">{{ Auth::user()->name }}</a>--}}

{{--        <div class="navbar-dropdown">--}}
{{--            <a class="navbar-item" href="{{ route('logout') }}"--}}
{{--               onclick="event.preventDefault();document.getElementById('logout-form').submit();">--}}
{{--                Logout--}}
{{--            </a>--}}

{{--            <form id="logout-form" action="{{ route('logout') }}" method="POST"--}}
{{--                  style="display: none;">--}}
{{--                {{ csrf_field() }}--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}
