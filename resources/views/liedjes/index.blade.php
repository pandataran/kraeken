@extends('layouts.app')

@section('content')
    <div class="container">
        <form id="save_post" method="POST" action="{{route('liedjes.store')}}">
            @csrf
            <div class="columns has-margin-top-30">
                <label for="name"><h2>Voeg nummer toe</h2></label>
            </div>

            <div class="columns has-margin-top-30">
                <input type="text" id="title" class="input is-fullwidth" name="titel" placeholder="Titel"
                       value="{{ old('titel') }}">
            </div>

            <div class="columns has-margin-top-30">
                <input type="text" id="artist" class="input is-fullwidth" name="artist" placeholder="Artist"
                       value="{{ old('artist') }}">
            </div>

            <div class="columns has-margin-top-30">
                <input type="duration_seconds" id="artist" class="input is-fullwidth" name="duration_seconds"
                       placeholder="Duratie" value="{{ old('duration_seconds') }}">
            </div>

            <div class="columns has-margin-top-30">
                <input type="submit" class="button is-success" value="Verstuur">
            </div>
        </form>

        <form method="get" action="{{route('liedjes.search')}}">
            <div class="columns has-margin-top-30">
                <input type="text" value="{{ app('request')->get('queryTitel') }}" name="queryTitel" class="input"
                       placeholder="Zoek Nummer">
            </div>
            <div class="columns has-margin-top-30">
                <input type="text" value="{{ app('request')->get('queryArtiest') }}"
                       name="queryArtiest" class="input"
                       placeholder="Zoek Artiest">
            </div>
            <div class="columns has-margin-top-30">
                <input type="submit" class="button is-success" value="Verstuur">
            </div>
        </form>

        <table class="table is-bordered is-fullwidth has-margin-top-30">
            <thead>
            @if (Auth::guest())
                <th>Artiest</th>
                <th>Titel</th>
                <th>Duur</th>
            </thead>
            <tbody>
            @forelse($liedjes as $key => $data)
                <tr>
                    <th>{{$data->artist}}</th>
                    <th>{{$data->title}}</th>
                    <th>{{$data->duration_seconds}}</th>
                </tr>
            @empty
                <tr>
                    <th colspan="4">Geen Liedjes gevonden</th>
                </tr>
            @endforelse
            </tbody>
        </table>
        @else
            <th>Artiest</th>
            <th>Titel</th>
            <th>Duur</th>
            <th></th>
            </thead>
            <tbody>
            @forelse($liedjes as $key => $data)
                <tr>
                    <th>{{$data->artist}}</th>
                    <th>{{$data->title}}</th>
                    <th>{{$data->duration_seconds}}</th>
                    <th><a href="{{ route('liedjes.destroy', $data->id) }}"
                           onclick="confirm('Weet je zeker dat je dit wil verwijderen?') && saveAndSubmit(event.preventDefault(),
                               document.getElementById('delete-form-{{$data->id}}').submit())">Verwijderen</a>
                        <script>
                            function saveAndSubmit(event) {
                                alert('Het lied is verwijderd');
                            }
                        </script>

                        <form id="delete-form-{{$data->id}}" action="{{ route('liedjes.destroy', $data->id) }}"
                              method="POST"
                              style="display: none;">
                            @csrf
                            @method('DELETE')
                        </form>
                    </th>
                </tr>
            @empty
                <tr>
                    <th colspan="3">Geen Liedjes gevonden
                    </th>
                </tr>
            @endforelse
            </tbody>
            </table>
        @endif
    </div>

@endsection
