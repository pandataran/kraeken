@extends('layouts.app')

@section('content')
    <div class="container">


        <table class="table is-bordered">
            <thead>
            @if (Auth::guest())
            <th>Programma</th>
            <th>Datum</th>
            <th>Tijd</th>
            <th>Duur in minuten</th>

                @else
                <th>Programma</th>
                <th>Datum</th>
                <th>Tijd</th>
                <th>Duur in minuten</th>
                <th></th>
                <th></th>
            @endif
            </thead>
            <tbody>
            @if (Auth::guest())
                @foreach($programmas as $key => $data)
                    <tr>
                        <th><p>{{$data->title}}</p></th>
                        <th><p>21-2</p></th>
                        <th><p>10:00 - 12:00</p></th>
                        <th><p>120</p></th>
                    </tr>
                @endforeach
            @else
                @foreach($programmas as $key => $data)
                    <tr>
                        <th><p>{{$data->title}}</p></th>
                        <th><p>21-2</p></th>
                        <th><p>10:00 - 12:00</p></th>
                        <th><p>120</p></th>
                        <th><a class="card-footer-item button is-warning">Wijzig</a></th>
                        <th><a class="card-footer-item button is-danger">Verwijder</a></th>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection
