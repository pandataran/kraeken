@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Auth::guest())
@else
        <form id="save_post" method="POST" action="{{route('zenders.store')}}">
            @csrf

            <div class="columns has-margin-top-30">
                <label for="name"><h2>Zender toevoegen</h2></label>
            </div>

            <div class="columns has-margin-top-30">
                <input type="text" id="title" class="input is-fullwidth" name="titel" placeholder="Titel"
                       value="{{ old('titel') }}">
            </div>

            <div class="columns has-margin-top-30">
                <input type="text" id="title" class="input is-fullwidth" name="omschrijving" placeholder="Omschrijving"
                       value="{{ old('omschrijving') }}">
            </div>

            <div class="columns has-margin-top-30">
                <input type="submit" class="button is-success" value="Verstuur">
            </div>

        </form>
        @endif
        <div class="columns has-margin-top-50">
            @forelse($zenders as $key => $data)
                @if($loop->index >= 2 && $loop->index % 3 == 0)</div>
        <div class="columns">@endif
            <div class="column is-one-third">
                <div class="card">

                    <div class="card-content">
                        <div class="content">
                            {{ substr(strip_tags($data->title), 0,120) }}{{ strlen(strip_tags($data->title)) >120 ? "..." : "" }}
                        </div>
                    </div>
                    @if (Auth::guest())
                        <footer class="card-footer">
                            <a href="{{route('zenders.show', $data->slug)}}" class="card-footer-item button is-info">Programmaoverzicht</a>
                        </footer>
                    @else
                        <footer class="card-footer">
                            <a href="{{route('zenders.show', $data->slug)}}" class="card-footer-item button is-info">Programmaoverzicht</a>
                        </footer>
                        <footer class="card-footer">
                            <a href="{{route('zenders.edit', $data->slug)}}" class="card-footer-item button is-warning">Wijzig</a>
                            <a class="card-footer-item button is-danger"
                               href="{{ route('zenders.destroy', $data->id) }}"
                               onclick="confirm('Weet je zeker dat je dit wil verwijderen?') && saveAndSubmit(event.preventDefault(),
                                   document.getElementById('delete-form-{{$data->id}}').submit())">Verwijderen</a>
                            <script>
                                function saveAndSubmit(event) {
                                    alert('De zender is verwijderd');
                                }
                            </script>

                            <form id="delete-form-{{$data->id}}" action="{{ route('zenders.destroy', $data->id) }}"
                                  method="POST"
                                  style="display: none;">
                                @csrf
                                @method('DELETE')
                            </form>
                        </footer>
                    @endif
                </div>
            </div>

            @empty
                <h3>Geen Zenders gevonden</h3>
            @endforelse
        </div>
    </div>

@endsection
