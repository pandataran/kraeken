@extends('layouts.app')

@section('content')
    <div class="container">
        <form id="save_post" method="POST" action="{{route('zenders.update', $zenders->slug)}}">
            @csrf
            @method('PATCh')
            <div class="columns has-margin-top-30">
                <label for="name"><h2>Zender toevoegen</h2></label>
            </div>

            <div class="columns has-margin-top-30">
                <input type="text" id="title" class="input is-fullwidth" name="titel" placeholder="Titel"
                       value="{{ $zenders->title }}">
            </div>

            <div class="columns has-margin-top-30">
                <input type="text" id="title" class="input is-fullwidth" name="omschrijving" placeholder="Omschrijving"
                       value="{{ $zenders->description }}">
            </div>

            <div class="columns has-margin-top-30">
                <input type="submit" class="button is-success" value="Verstuur">
            </div>

        </form>
    </div>
@endsection
