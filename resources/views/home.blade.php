@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="columns has-margin-top-100">
            <div class="column is-one-third">
                <figure class="image">
                    <img src="{{ asset('images/1.jpg') }}">
                </figure>
            </div>
            <div class="column is-one-third">
                <figure class="image">
                    <img src="{{ asset('images/3.jpg') }}">
                </figure>
            </div>
            <div class="column is-one-third">
                <figure class="image">
                    <img src="{{ asset('images/1.jpg') }}">
                </figure>
            </div>
        </div>
    </div>
    <nav class="navbar is-link has-margin-top-100">
        <div class="navbar-brand">
            <a class="navbar-item">Copyright Kraeken & Krønen</a>
        </div>
    </nav>

@endsection
