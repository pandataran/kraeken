<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Zenders extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'title', 'description', 'slug'
    ];

    public function programmas()
    {
        return $this->hasMany('App\Programmas', 'zenders_id');
    }
}
