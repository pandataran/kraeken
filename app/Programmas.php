<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Programmas extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'title', 'slug', 'start_time', 'end_time', 'date', 'zenders_id'
    ];

    public function zender()
    {
        return $this->hasOne('App\Zenders', 'id', 'zenders_id');
    }
}
