<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Liedjes extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id', 'title', 'slug', 'artist', 'duration_seconds'
    ];

}
