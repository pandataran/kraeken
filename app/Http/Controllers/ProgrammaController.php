<?php

namespace App\Http\Controllers;

use App\Programmas;
use App\Zenders;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;


class ProgrammaController extends Controller
{

    public function index()
    {
        $programmas = Programmas::orderBy('title')->paginate(env('APP_PAGINATION_LIMIT', 5));
        $zenders = Zenders::all();

        return view('programmas.index', compact('programmas', 'zenders'));
    }

    public function store(Request $request)
    {
        //Data Valideren en opvragen van de form
        $this->validate(
            $request, [
            'titel' => 'required|min:1|unique:programmas,title,NULL,id,deleted_at,NULL',
            'zenders_id' => 'required|exists:zenders,id'
        ])
        ;

        //Aangeven waar de data moet worden opgeslagen in de database
        $programmas = Programmas::create([
            'title' => $request->titel,
            'slug' => Str::slug($request->titel),
            'zenders_id' => $request->zenders_id
        ]);

        return redirect()->route('programmas.index', $programmas->slug)->with('success', 'Programma aangemaakt');

    }

    public function show($id)
    {
        // Zet de slug variable als url
        $slug = strtolower($id);
        $programmas = Programmas::where('slug', '=', $slug)->get()->first();
        if (!$programmas) {
            abort(404);
        }
        return view('programmas.show', compact('programmas'));

    }

    public function edit($id)
    {
        // Zet de slug variable als url
        $slug = strtolower($id);
        $zenders = Zenders::all();
        $programmas = Programmas::where('slug', '=', $slug)->get()->first();
        if (!$programmas) {
            abort(404);
        }
        return view('programmas.edit', compact('programmas', 'zenders'));

    }

    public function update(Request $request, $id)
    {
        //Data Valideren en opvragen van de form
        $this->validate(
            $request, [
            'titel' => 'required|min:1|unique:programmas,title,NULL,id,deleted_at,NULL' . $id,
            'zenders_id' => 'required|exists:zenders,id'
        ]);

        $slug = strtolower($id);
        $programmas = Programmas::where('slug', '=', $slug)->get()->first();
        if (!$programmas) {
            abort(404);
        }

        //Aangeven waar de data moet worden opgeslagen in de database
        $programmas->title = $request->titel;
        $programmas->slug = Str::slug($request->titel, '-');
        $programmas->zenders_id = $request->zenders_id;

        //Data opslaan in database
        $programmas->save();

        return redirect()->route('programmas.index')->with('success', 'Programma bewerkt');

    }
}
