<?php

namespace App\Http\Controllers;

use App\Liedjes;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function liedjes()
    {
        $liedjes = Liedjes::all();
        return view('liedjes.index', compact('liedjes'));
    }

    public function zenders()
    {
        return view('zenders.index');
    }

    public function programmas()
    {
        return view('programmas.index');
    }

    public function contact()
    {
        return view('contact');
    }
}
