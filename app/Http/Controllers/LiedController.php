<?php

namespace App\Http\Controllers;

use App\Liedjes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class LiedController extends Controller
{
    public function index()
    {
        $liedjes = Liedjes::orderBy('artist')->paginate(env('APP_PAGINATION_LIMIT', 5));
        return view('liedjes.index', compact('liedjes'));
    }

    public function store(Request $request)
    {
        //Data Valideren en opvragen van de form
        $this->validate(
            $request, [
            'titel' => 'required|min:1|unique:liedjes,title,NULL,id,deleted_at,NULL',
            'artist' => 'required|min:1',
            'duration_seconds' => 'required|min:1',

        ]);

        //Aangeven waar de data moet worden opgeslagen in de database
        $liedjes = Liedjes::create([
            'title' => $request->titel,
            'artist' => Str::slug($request->artist),
            'slug' => Str::slug($request->titel),
            'duration_seconds' => $request->duration_seconds
        ]);

        //Data opslaan in database
        $liedjes->save();

        return redirect()->route('liedjes.index')->with('success', 'Lied aangemaakt.');
    }

    public function show($id)
    {
        // Zet de slug variable als url
        $slug = strtolower($id);
        $liedjes = Liedjes::where('slug', '=', $slug)->get()->first();
        if (!$liedjes) {
            abort(404);
        }

        return view('liedjes.show', compact('liedjes'));
    }

    public function search(Request $request)
    {
        $liedjes = new Collection();
        $queryTitel = $request->get('queryTitel');
        $queryArtiest = $request->get('queryArtiest');
        if ($queryTitel || $queryArtiest) {
            $liedjesQuery = Liedjes::query();

            if ($queryTitel) {
                $liedjesQuery->where('title', 'like', '%' . $queryTitel . '%');
            }
            if ($queryArtiest) {
                $liedjesQuery->where('artist', 'like', '%' . $queryArtiest . '%');
            }
            $liedjes = $liedjesQuery->get();
        }
        return view('liedjes.index', compact('liedjes'));
    }

    public function destroy($id)
    {
        // Zet de slug variable als url
        $lied_id = strtolower($id);
        $liedjes = Liedjes::where('id', '=', $lied_id)->get()->first();
        if (!$liedjes) {
            abort(404);
        }

        $response = "Je hebt {$liedjes->title} verwijderd";
        $liedjes->delete();

        return redirect()->route('liedjes.index')->with('deleted', $response);
    }
}
