<?php

namespace App\Http\Controllers;

use App\Programmas;
use App\Zenders;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class ZenderController extends Controller
{

    public function index()
    {
        $zenders = Zenders::orderBy('title')->paginate(env('APP_PAGINATION_LIMIT', 5));
        return view('zenders.index', compact('zenders'));
    }

    public function store(Request $request)
    {
        //Data Valideren en opvragen van de form
        $this->validate(
            $request, [
            'titel' => 'required|min:1|unique:zenders,title,NULL,id,deleted_at,NULL',
            'omschrijving' => 'required'
        ]);

        //Aangeven waar de data moet worden opgeslagen in de database
        $zenders = Zenders::create([
            'title' => $request->titel,
            'description' => $request->omschrijving,
            'slug' => Str::slug($request->titel),
        ]);

        //Data opslaan in database
        $zenders->save();

        return redirect()->route('zenders.index', $zenders->slug)->with('success', 'Zender aangemaakt');

    }

    public function show($id)
    {
        // Zet de slug variable als url
        $slug = strtolower($id);
        $programmas = Programmas::all();
        $zenders = Zenders::where('slug', '=', $slug)->get()->first();
        if (!$zenders) {
            abort(404);
        }
        return view('zenders.show', compact('zenders', 'programmas'));

    }

    public function edit($id)
    {
        // Zet de slug variable als url
        $slug = strtolower($id);
        $zenders= Zenders::where('slug', '=', $slug)->get()->first();
        if (!$zenders) {
            abort(404);
        }
        return view('zenders.edit', compact('zenders'));

    }

    public function update(Request $request, $id)
    {
        //Data Valideren en opvragen van de form
        $this->validate(
            $request, [
            'titel' => 'required|min:1|unique:zenders,title,NULL,id,deleted_at,NULL'. $id,
            'omschrijving' => 'required'
        ]);

        $slug = strtolower($id);
        $zenders = Zenders::where('slug', '=', $slug)->get()->first();
        if (!$zenders) {
            abort(404);
        }

        //Aangeven waar de data moet worden opgeslagen in de database
        $zenders->title = $request->titel;
        $zenders->description = $request->omschrijving;
        $zenders->slug = Str::slug($request->titel, '-');

        //Data opslaan in database
        $zenders->save();

        return redirect()->route('zenders.index')->with('success', 'Zender bewerkt');
    }

    public function destroy($id)
    {
        // Zet de slug variable als url
        $id = strtolower($id);
        $zenders = Zenders::where('id', '=', $id)->get()->first();
        if (!$zenders) {
            abort(404);
        }

        $response = "Je hebt  $zenders->title} verwijderd";
        $zenders->delete();

        return redirect()->route('zenders.index')->with('deleted', $response);
    }
}
