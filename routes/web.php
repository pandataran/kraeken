<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PageController@home');
Route::get('/home', 'PageController@home');
Route::get('/zenders', 'PageController@zenders');
Route::get('/contact', 'PageController@contact');
Route::get('liedjes/search', 'LiedController@search')->name('liedjes.search');
Route::resource('zenders', 'ZenderController');
Route::resource('programmas', 'ProgrammaController');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/liedjes', 'PageController@liedjes');
    Route::resource('liedjes', 'LiedController');

});

Auth::routes();

